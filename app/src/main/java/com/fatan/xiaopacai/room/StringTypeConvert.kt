package com.fatan.xiaopacai.room

import androidx.room.TypeConverter

/**
 * @author: WangYeTong
 * @date: 2023/12/27 17:36
 * @remake: 随机摇骰子集合的转换类
 */
class RandomShaiZiTypeConvert {
    @TypeConverter
    fun fromList(list: List<Int>): String {
        return list.joinToString(",")
    }

    @TypeConverter
    fun toList(data: String): List<Int> {
        return try {
            data.split(",").map { it.toInt() }
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }
}