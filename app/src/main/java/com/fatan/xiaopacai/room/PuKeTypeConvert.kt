package com.fatan.xiaopacai.room

import androidx.room.TypeConverter
import com.fatan.xiaopacai.mvvm.model.PuKe
import com.fatan.xiaopacai.util.JsonUtil

/**
 * @author: WangYeTong
 * @date: 2023/12/28 18:09
 * @remake:扑克集合的转换类
 */
class PuKeTypeConvert {
    @TypeConverter
    fun fromList(list: List<PuKe>): String {
        return JsonUtil.toJson(list) ?: ""
    }

    @TypeConverter
    fun toList(data: String): List<PuKe> {
        try {
            return JsonUtil.jsonToList(data, PuKe::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return listOf()
    }
}