package com.fatan.xiaopacai.room.randomshaizi

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author: WangYeTong
 * @date: 2023/12/27 17:30
 * @remake:
 */
@Entity
data class RandomShaiZiEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val shaiZiList: List<Int>,
    @ColumnInfo
    val timeMill: Long,
)