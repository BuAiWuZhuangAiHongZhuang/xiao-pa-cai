package com.fatan.xiaopacai.room.tenhalf

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fatan.xiaopacai.mvvm.model.PuKe

/**
 * @author: WangYeTong
 * @date: 2024/1/3 10:29
 * @remake: 十点半
 */
@Entity
data class TenHalfEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @ColumnInfo
    var timeMill: Long,
    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    var pukeList: List<PuKe>
)