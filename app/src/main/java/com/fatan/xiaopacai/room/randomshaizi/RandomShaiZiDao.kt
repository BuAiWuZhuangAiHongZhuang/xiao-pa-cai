package com.fatan.xiaopacai.room.randomshaizi

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

/**
 * @author: WangYeTong
 * @date: 2023/12/27 17:40
 * @remake:
 */
@Dao
interface RandomShaiZiDao {
    @Query("select * from RandomShaiZiEntity")
    suspend fun getAll(): List<RandomShaiZiEntity>

    @Insert
    suspend fun insertAll(vararg entity: RandomShaiZiEntity)

    @Delete
    suspend fun delete(entity: RandomShaiZiEntity)
}