package com.fatan.xiaopacai.room.randompuke

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fatan.xiaopacai.mvvm.model.PuKe

/**
 * @author: WangYeTong
 * @date: 2023/12/28 18:05
 * @remake:
 */
@Entity
data class RandomPuKeEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(typeAffinity = ColumnInfo.TEXT)
    val puKeList: List<PuKe>,
    @ColumnInfo
    val timeMill: Long,
)