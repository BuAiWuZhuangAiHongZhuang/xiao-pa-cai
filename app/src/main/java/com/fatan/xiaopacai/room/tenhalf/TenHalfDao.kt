package com.fatan.xiaopacai.room.tenhalf

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.fatan.xiaopacai.room.randompuke.RandomPuKeEntity

/**
 * @author: WangYeTong
 * @date: 2024/1/3 10:31
 * @remake:
 */
@Dao
interface TenHalfDao {
    @Query("select * from TenHalfEntity")
    suspend fun getAll(): List<TenHalfEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity: TenHalfEntity):Long

    @Update
    suspend fun update(entity: TenHalfEntity)

    @Delete
    suspend fun delete(entity: TenHalfEntity)
}