package com.fatan.xiaopacai.room.randompuke

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

/**
 * @author: WangYeTong
 * @date: 2023/12/28 18:12
 * @remake:
 */
@Dao
interface RandomPuKeDao {
    @Query("select * from RandomPuKeEntity")
    suspend fun getAll(): List<RandomPuKeEntity>

    @Insert
    suspend fun insertAll(vararg entity: RandomPuKeEntity)

    @Delete
    suspend fun delete(entity: RandomPuKeEntity)
}