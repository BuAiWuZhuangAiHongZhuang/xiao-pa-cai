package com.fatan.xiaopacai.room

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.fatan.xiaopacai.base.BaseApplication
import com.fatan.xiaopacai.room.randompuke.RandomPuKeDao
import com.fatan.xiaopacai.room.randompuke.RandomPuKeEntity
import com.fatan.xiaopacai.room.randomshaizi.RandomShaiZiDao
import com.fatan.xiaopacai.room.randomshaizi.RandomShaiZiEntity
import com.fatan.xiaopacai.room.tenhalf.TenHalfDao
import com.fatan.xiaopacai.room.tenhalf.TenHalfEntity

/**
 * @author: WangYeTong
 * @date: 2023/12/27 17:51
 * @remake:
 */
@Database(
    version = 1,
    exportSchema = false,
    entities = [RandomShaiZiEntity::class, RandomPuKeEntity::class, TenHalfEntity::class]
)
@TypeConverters(RandomShaiZiTypeConvert::class, PuKeTypeConvert::class)
abstract class AppDataBase : RoomDatabase() {
    abstract fun randomShaiZiDao(): RandomShaiZiDao
    abstract fun randomPuKeDao(): RandomPuKeDao
    abstract fun tenHalfDao(): TenHalfDao

    companion object {
        val database by lazy {
            Room.databaseBuilder(
                BaseApplication.INSTANCE,
                AppDataBase::class.java,
                "AppDataBase"
            ).build()
        }
    }
}