package com.fatan.xiaopacai.base

/**
 * @author: WangYeTong
 * @date: 2023/12/20 14:54
 * @remake:
 */
const val TAG = "XiaoPaCaiTag"
const val FORMAT_0 = "yyyy:MM:dd-HH:mm:ss:mmmm"
