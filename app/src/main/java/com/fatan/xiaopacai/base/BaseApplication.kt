package com.fatan.xiaopacai.base

import android.app.Application

/**
 * @author: WangYeTong
 * @date: 2023/12/22 11:11
 * @remake:
 */
class BaseApplication : Application() {
    companion object {
        lateinit var INSTANCE: Application
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }
}