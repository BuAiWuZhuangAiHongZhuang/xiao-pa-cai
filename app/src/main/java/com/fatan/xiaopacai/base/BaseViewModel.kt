package com.fatan.xiaopacai.base

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * @author: WangYeTong
 * @date: 2023/12/20 11:11
 * @remake:
 */
open class BaseViewModel : ViewModel() {
    protected val isLoading = mutableStateOf(false)

    protected fun serverAwait(block: suspend CoroutineScope.() -> Unit) =
        viewModelScope.launch(Dispatchers.IO) {
            isLoading.value = true
            block.invoke(this)
            isLoading.value = false
        }

    fun sleepTime(millis: Long = 1500, block: () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            Thread.sleep(millis)
            block()
        }
    }
}