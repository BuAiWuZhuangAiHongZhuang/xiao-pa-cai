package com.fatan.xiaopacai.mvvm.viewmodel

import androidx.compose.runtime.mutableStateOf
import com.fatan.xiaopacai.base.BaseViewModel
import com.fatan.xiaopacai.mvvm.model.MainNav

/**
 * @author: WangYeTong
 * @date: 2023/12/26 17:10
 * @remake:
 */
class MainViewModel : BaseViewModel() {
    /**
     * 当前页面
     */
    val bottomNav = mutableStateOf<MainNav.BottomNav>(MainNav.BottomNav.Game)
    val navs = listOf(
        MainNav.BottomNav.Game,
        MainNav.BottomNav.Mine,
    )
}