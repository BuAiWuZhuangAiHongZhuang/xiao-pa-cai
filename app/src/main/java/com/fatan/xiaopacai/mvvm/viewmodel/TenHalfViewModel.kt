package com.fatan.xiaopacai.mvvm.viewmodel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.fatan.xiaopacai.base.BaseViewModel
import com.fatan.xiaopacai.base.TAG
import com.fatan.xiaopacai.mvvm.model.PuKe
import com.fatan.xiaopacai.mvvm.model.allPuKe
import com.fatan.xiaopacai.room.AppDataBase
import com.fatan.xiaopacai.room.tenhalf.TenHalfEntity
import com.fatan.xiaopacai.util.JsonUtil

/**
 * @author: WangYeTong
 * @date: 2024/1/2 15:32
 * @remake:
 */
class TenHalfViewModel : BaseViewModel() {
    private val _pukeDataList = mutableStateOf<List<PuKe>>(listOf())
    val pukeDataList: MutableState<List<PuKe>>
        get() = _pukeDataList
    private val _showDialog = mutableStateOf(false)
    val showDialog: MutableState<Boolean>
        get() = _showDialog
    private val _showDialogTip = mutableStateOf("")
    val showDialogTip: MutableState<String>
        get() = _showDialogTip

    private var recordEntity: TenHalfEntity? = null

    /**
     * 抽取一张随机牌
     */
    fun addRandomPuke() {
        val tempDataList = _pukeDataList.value.toMutableList()
        if (checkShowDialog()) return
        val puke = allPuKe.random()
        if (!tempDataList.contains(puke)) {
            tempDataList.add(puke)
            _pukeDataList.value = tempDataList
            addOrUpdateRecord()
        } else {
            addRandomPuke()
        }
    }

    fun reset() {
        recordEntity = null
        _pukeDataList.value = listOf()
    }

    fun getGridCell() = when (_pukeDataList.value.size) {
        0, 1 -> 1
        2 -> 2
        else -> 3
    }

    fun setShowDialog(showDialog: Boolean) {
        _showDialog.value = showDialog
    }

    /**
     * 抽卡，加入数据库
     */
    private fun addOrUpdateRecord() {
        if (_pukeDataList.value.isEmpty()) {
            return
        }
        if (recordEntity == null) {
            recordEntity = TenHalfEntity(timeMill = System.currentTimeMillis(), pukeList = _pukeDataList.value)
        } else {
            recordEntity?.pukeList = _pukeDataList.value
            recordEntity?.timeMill = System.currentTimeMillis()
        }
        Log.i(TAG, "addRecord: ${JsonUtil.toJson(recordEntity)}")
        serverAwait {
            val id = AppDataBase.database.tenHalfDao().insert(recordEntity!!)
            Log.i(TAG, "addOrUpdateRecord: insert id:$id")
            recordEntity?.id = id.toInt()
        }
    }

    private fun checkShowDialog(): Boolean {
        if (_pukeDataList.value.size >= 5) {
            _showDialogTip.value = "最多抽取五张，请重新开始游戏"
            _showDialog.value = true
            recordEntity = null
            return true
        }
        var num = 0f
        for (item in _pukeDataList.value) {
            if (item.value >= 11) {
                num += 0.5f
            } else {
                num += item.value
            }
        }
        if (num > 10.5) {
            _showDialogTip.value = "你已经老了！"
            _showDialog.value = true
            recordEntity = null
            return true
        }

        if (num == 10.5f) {
            _showDialogTip.value = "你都10点半了，还要抽？"
            _showDialog.value = true
            recordEntity = null
            return true
        }

        _showDialog.value = false
        return false
    }

}