package com.fatan.xiaopacai.mvvm.model

import com.fatan.xiaopacai.R

/**
 * @author: WangYeTong
 * @date: 2024/1/18 14:45
 * @remake:
 */

sealed class CaiQuanType(val id: Int, val name: String, val img: Int) {
    object JianDao : CaiQuanType(0, "剪刀", R.drawable.jiandao)
    object ShiTou : CaiQuanType(1, "石头", R.drawable.shitou)
    object Bu : CaiQuanType(2, "布", R.drawable.bu)
}

val caiQuanTypeList by lazy {
    listOf(CaiQuanType.JianDao, CaiQuanType.ShiTou, CaiQuanType.Bu)
}