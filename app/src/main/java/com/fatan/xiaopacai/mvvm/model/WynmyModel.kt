package com.fatan.xiaopacai.mvvm.model

/**
 * @author: WangYeTong
 * @date: 2024/1/18 15:07
 * @remake:
 */
val wynmyGameRuler by lazy {
    "说你一件你做过的事情，没有做过的人喝酒。如果在场所有人都做过，则你需要喝在场人数杯的酒。（请不要说自己职业技能，如我写过Android代码）"
}
