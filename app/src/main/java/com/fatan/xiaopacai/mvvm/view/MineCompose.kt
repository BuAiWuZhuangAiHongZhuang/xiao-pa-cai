package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fatan.xiaopacai.mvvm.viewmodel.GameViewModel
import com.fatan.xiaopacai.mvvm.viewmodel.MineViewModel
import com.fatan.xiaopacai.ui.theme.themeColor

/**
 * @author: WangYeTong
 * @date: 2023/12/26 17:23
 * @remake:
 */
@Composable
fun MineCompose(
    modifier: Modifier = Modifier,
    mainViewModel: MineViewModel = viewModel(),
    navController: NavHostController = rememberNavController()
) {
    Column(modifier = modifier.fillMaxSize()) {
        TitleBar(isBack = false, titleText = "我的")
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = "本应用仅用于内部测试，无任何商业性质。如有更好的建议，请发邮箱:wytaper495@qq.com联系，谢谢使用",
                fontSize = 22.sp,
                color = Color.Red,
                modifier = Modifier.padding(30.dp)
            )
        }
    }
}