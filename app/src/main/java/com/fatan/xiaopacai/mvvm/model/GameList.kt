package com.fatan.xiaopacai.mvvm.model

/**
 * @author: WangYeTong
 * @date: 2023/12/26 17:35
 * @remake:
 */
sealed class GameList(val id: Int, val name: String) {
    object GameRandomShaiZi : GameList(0, "随机摇骰子")
    object GameRandomPuke : GameList(1, "随机扑克")
    object GameMeiNvChanShen : GameList(2, "美女缠身")
    object TenHalf : GameList(3, "10点半")
    object GameXiaoJiePuke : GameList(4, "小姐牌-以酒坑友")
    object GameCaiQuan : GameList(5, "剪刀石头布")
    object GameWynmy : GameList(6, "我有你没有")
    object GameQiBaJiu : GameList(7, "七八九")
}

/**
 * 所有游戏列表
 */
val gameList = listOf(
    GameList.GameRandomShaiZi,
    GameList.GameRandomPuke,
    GameList.GameMeiNvChanShen,
    GameList.TenHalf,
    GameList.GameXiaoJiePuke,
    GameList.GameCaiQuan,
    GameList.GameWynmy,
    GameList.GameQiBaJiu,
)