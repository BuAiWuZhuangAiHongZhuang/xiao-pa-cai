package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fatan.xiaopacai.mvvm.model.PuKe
import com.fatan.xiaopacai.mvvm.model.RandomPuKeMaxDbNum
import com.fatan.xiaopacai.mvvm.viewmodel.RandomPuKeRecordViewModel

/**
 * @author: WangYeTong
 * @date: 2023/12/28 18:16
 * @remake: 随机扑克的历史记录
 */
@Composable
fun RandomPuKeRecordCompose(
    navHostController: NavHostController, randomPuKeRecordViewModel: RandomPuKeRecordViewModel = viewModel()
) {
    val allRecordData by remember { randomPuKeRecordViewModel.allRecordData }
    LaunchedEffect(key1 = Unit) {
        randomPuKeRecordViewModel.getAllRecord()
    }
    Column(modifier = Modifier.fillMaxSize()) {
        TitleBar(titleText = "历史记录最新${RandomPuKeMaxDbNum}条", backClick = { navHostController.navigateUp() })
        LazyColumn(contentPadding = PaddingValues(16.dp)) {
            itemsIndexed(allRecordData) { index, item ->
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(horizontal = 20.dp)
                ) {
                    Text(text = "序号:${index + 1}", fontSize = 18.sp, color = Color.White)
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.CenterEnd) {
                        Text(text = "${randomPuKeRecordViewModel.getFormatDateString(item.timeMill)}", color = Color.White, fontSize = 18.sp)
                    }
                }
                LazyHorizontalGrid(GridCells.Fixed(1), modifier = Modifier.height(200.dp), contentPadding = PaddingValues(16.dp)) {
                    itemsIndexed(item.puKeList) { puKeIndex: Int, puKeItem: PuKe ->
                        Image(
                            painter = painterResource(id = puKeItem.img), contentDescription = null
                        )
                    }
                }
            }
        }
    }
}

@Composable
@Preview
fun RandomPuKeRecordPreview() {
    Surface(modifier = Modifier.fillMaxWidth()) {
        RandomPuKeRecordCompose(rememberNavController())
    }
}