package com.fatan.xiaopacai.mvvm.model

import com.fatan.xiaopacai.R

/**
 * @author: WangYeTong
 * @date: 2023/12/28 17:04
 * @remake:
 */
val maxPuKeNum by lazy { allPuKe.size }
val minPuKeNum by lazy { 1 }

/**
 * 随机扑克数据库中最大数据数
 */
const val RandomPuKeMaxDbNum = 100

val allPuKe by lazy {
    listOf(
        PuKe(11, "黑桃A", R.mipmap.puke_1_1, 1),
        PuKe(12, "红桃A", R.mipmap.puke_1_2, 1),
        PuKe(13, "梅花A", R.mipmap.puke_1_3, 1),
        PuKe(14, "方块A", R.mipmap.puke_1_4, 1),

        PuKe(21, "黑桃2", R.mipmap.puke_2_1, 2),
        PuKe(22, "红桃2", R.mipmap.puke_2_2, 2),
        PuKe(23, "梅花2", R.mipmap.puke_2_3, 2),
        PuKe(24, "方块2", R.mipmap.puke_2_4, 2),

        PuKe(31, "黑桃3", R.mipmap.puke_3_1, 3),
        PuKe(32, "红桃3", R.mipmap.puke_3_2, 3),
        PuKe(33, "梅花3", R.mipmap.puke_3_3, 3),
        PuKe(34, "方块3", R.mipmap.puke_3_4, 3),

        PuKe(41, "黑桃4", R.mipmap.puke_4_1, 4),
        PuKe(42, "红桃4", R.mipmap.puke_4_2, 4),
        PuKe(43, "梅花4", R.mipmap.puke_4_3, 4),
        PuKe(44, "方块4", R.mipmap.puke_4_4, 4),

        PuKe(51, "黑桃5", R.mipmap.puke_5_1, 5),
        PuKe(52, "红桃5", R.mipmap.puke_5_2, 5),
        PuKe(53, "梅花5", R.mipmap.puke_5_3, 5),
        PuKe(54, "方块5", R.mipmap.puke_5_4, 5),

        PuKe(61, "黑桃6", R.mipmap.puke_6_1, 6),
        PuKe(62, "红桃6", R.mipmap.puke_6_2, 6),
        PuKe(63, "梅花6", R.mipmap.puke_6_3, 6),
        PuKe(64, "方块6", R.mipmap.puke_6_4, 6),

        PuKe(71, "黑桃7", R.mipmap.puke_7_1, 7),
        PuKe(72, "红桃7", R.mipmap.puke_7_2, 7),
        PuKe(73, "梅花7", R.mipmap.puke_7_3, 7),
        PuKe(74, "方块7", R.mipmap.puke_7_4, 7),

        PuKe(81, "黑桃8", R.mipmap.puke_8_1, 8),
        PuKe(82, "红桃8", R.mipmap.puke_8_2, 8),
        PuKe(83, "梅花8", R.mipmap.puke_8_3, 8),
        PuKe(84, "方块8", R.mipmap.puke_8_4, 8),

        PuKe(91, "黑桃9", R.mipmap.puke_9_1, 9),
        PuKe(92, "红桃9", R.mipmap.puke_9_2, 9),
        PuKe(93, "梅花9", R.mipmap.puke_9_3, 9),
        PuKe(94, "方块9", R.mipmap.puke_9_4, 9),

        PuKe(101, "黑桃10", R.mipmap.puke_10_1, 10),
        PuKe(102, "红桃10", R.mipmap.puke_10_2, 10),
        PuKe(103, "梅花10", R.mipmap.puke_10_3, 10),
        PuKe(104, "方块10", R.mipmap.puke_10_4, 10),

        PuKe(111, "黑桃J", R.mipmap.puke_11_1, 11),
        PuKe(112, "红桃J", R.mipmap.puke_11_2, 11),
        PuKe(113, "梅花J", R.mipmap.puke_11_3, 11),
        PuKe(114, "方块J", R.mipmap.puke_11_4, 11),

        PuKe(121, "黑桃Q", R.mipmap.puke_12_1, 12),
        PuKe(122, "红桃Q", R.mipmap.puke_12_2, 12),
        PuKe(123, "梅花Q", R.mipmap.puke_12_3, 12),
        PuKe(124, "方块Q", R.mipmap.puke_12_4, 12),

        PuKe(131, "黑桃K", R.mipmap.puke_13_1, 13),
        PuKe(132, "红桃K", R.mipmap.puke_13_2, 13),
        PuKe(133, "梅花K", R.mipmap.puke_13_3, 13),
        PuKe(134, "方块K", R.mipmap.puke_13_4, 13),

        PuKe(141, "小王", R.mipmap.puke_14_1, 14),
        PuKe(151, "大王", R.mipmap.puke_15_1, 15),
    )
}

data class PuKe(val id: Int, val name: String, val img: Int, val value: Int)

