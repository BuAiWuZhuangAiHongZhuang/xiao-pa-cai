package com.fatan.xiaopacai.mvvm.view

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

/**
 * @author: WangYeTong
 * @date: 2023/12/25 10:44
 * @remake:页面导航管理
 */
@Composable
fun NavigationHost(navHostController: NavHostController = rememberNavController()) {
    NavHost(
        navController = navHostController,
        startDestination = Nav.Main.route,
    ) {
        //主页
        composable(route = Nav.Main.route) {
            MainCompose(navController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //随机摇骰子
        composable(route = Nav.RandomShaiZi.route) {
            RandomShaiZiCompose(navController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //随机摇骰子历史记录
        composable(route = Nav.RandomShaiZiRecord.route) {
            RandomShaiZiRecordCompose(navHostController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //随机扑克
        composable(route = Nav.RandomPuKe.route) {
            RandomPuKeCompose(navController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //随机扑克历史记录
        composable(route = Nav.RandomPuKeRecord.route) {
            RandomPuKeRecordCompose(navHostController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //美女缠身
        composable(route = Nav.MeiNvChanShen.route) {
            MeiNvChanShenCompose(navHostController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //10点半
        composable(route = Nav.TenHalf.route) {
            TenHalfCompose(navHostController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //十点半历史记录
        composable(route = Nav.TenHalfRecord.route) {
            TenHalfRecordCompose(navHostController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //小姐牌
        composable(route = Nav.XiaoJiePuke.route) {
            XiaoJiePukeCompose(navHostController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //小姐牌规则
        composable(route = Nav.XiaoJiePukeRuler.route) {
            XiaoJiePukeRulerCompose(navHostController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }

        //猜拳
        composable(route = Nav.CaiQuan.route) {
            CaiQuanCompose(navHostController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }

        //我有你没有
        composable(route = Nav.Wynmy.route) {
            WynmyCompose(navHostController = navHostController)
            BackHandler { navHostController.navigateUp() }
        }
        //七八九
        composable(route = Nav.QiBaJiu.route) {
            QiBaJiuCompose(navHostController = navHostController)
            BackHandler {
                navHostController.navigateUp()
            }
        }
    }
}

sealed class Nav(val route: String) {
    object Main : Nav("Main")
    object RandomShaiZi : Nav("RandomShaiZi")
    object RandomShaiZiRecord : Nav("RandomShaiZiRecord")
    object RandomPuKe : Nav("RandomPuKe")
    object RandomPuKeRecord : Nav("RandomPuKeRecord")
    object MeiNvChanShen : Nav("MeiNvChanShen")
    object TenHalf : Nav("TenHalf")
    object TenHalfRecord : Nav("TenHalfRecord")
    object XiaoJiePuke : Nav("XiaoJiePuke")
    object XiaoJiePukeRuler : Nav("XiaoJiePukeRuler")
    object CaiQuan : Nav("CaiQuan")
    object Wynmy : Nav("Wynmy")
    object QiBaJiu : Nav("QiBaJiu")
}