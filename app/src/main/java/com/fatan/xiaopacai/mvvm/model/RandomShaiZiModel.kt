package com.fatan.xiaopacai.mvvm.model

import com.fatan.xiaopacai.R

/**
 * @author: WangYeTong
 * @date: 2023/12/27 10:25
 * @remake:
 */
val maxShaiZiNum = 6
val minShaiZiNum = 1

/**
 * 随机摇色子数据库中最大数据数。
 */
const val RandomShaiZiMaxDbNum = 100

/**
 * 对应点数获取骰子图片
 */
fun getImgByShaiZiNum(num: Int): Int {
    return when (num) {
        1 -> R.drawable.shaizi1
        2 -> R.drawable.shaizi2
        3 -> R.drawable.shaizi3
        4 -> R.drawable.shaizi4
        5 -> R.drawable.shaizi5
        6 -> R.drawable.shaizi6
        else -> R.drawable.shaizi1
    }
}