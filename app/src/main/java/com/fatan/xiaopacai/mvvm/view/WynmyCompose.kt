package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.fatan.xiaopacai.mvvm.model.wynmyGameRuler
import com.fatan.xiaopacai.mvvm.viewmodel.WynmyViewModel

/**
 * @author: WangYeTong
 * @date: 2024/1/18 15:05
 * @remake: 我有你没有
 */
@Composable
fun WynmyCompose(
    navHostController: NavHostController,
    wynmyViewModel: WynmyViewModel = viewModel()
) {
    Column {
        TitleBar(titleText = "我有你没有", backClick = {
            navHostController.navigateUp()
        })

        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(12.dp), contentAlignment = Alignment.Center
        ) {
            Text(text = wynmyGameRuler, color = Color.White, fontSize = 22.sp, textAlign = TextAlign.Center)
        }
    }


}