package com.fatan.xiaopacai.mvvm.model

/**
 * @author: WangYeTong
 * @date: 2024/1/18 15:17
 * @remake:
 */

val qibaJiuGameRuler by lazy {
    "把两粒色子放在一个杯子里，轮流摇，摇到1、3、4、5、6都不用喝，摇到7，不用喝酒，但要往公共杯里面加酒，想加多少就加多少，然后到下一个，摇到8就要把公共杯里的酒喝一半，然后继续摇，摇到9就要喝完，然后随意倒酒再继续摇，摇到两个1可以指定在座的任何一个人喝玩这杯酒，如果摇到两个相同的点子，摇色子的顺序就倒转一下。"
}