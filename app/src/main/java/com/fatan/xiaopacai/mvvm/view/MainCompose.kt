package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fatan.xiaopacai.mvvm.model.MainNav
import com.fatan.xiaopacai.mvvm.viewmodel.MainViewModel

/**
 * @author: WangYeTong
 * @date: 2023/12/20 11:02
 * @remake:
 */
@Composable
fun MainCompose(
    modifier: Modifier = Modifier,
    mainViewModel: MainViewModel = viewModel(),
    navController: NavHostController = rememberNavController()
) {
    var currentBottomNav by remember { mainViewModel.bottomNav }
    val navs = mainViewModel.navs
    Column(modifier = modifier) {
        //页面
        MainPage(
            currentBottomNav,
            modifier = Modifier.weight(1f),
            navController = navController
        )
        //底部导航栏
        MainBottomBar(
            navs,
            currentBottomNav,
            modifier = Modifier.height(55.dp)
        ) { bottomNav ->
            currentBottomNav = bottomNav
        }
    }
}

@Composable
@Preview
fun MainComposePreview() {
    Surface(modifier = Modifier.fillMaxSize()) {
        MainCompose()
    }
}

@Composable
fun MainPage(bottomNav: MainNav.BottomNav, modifier: Modifier = Modifier, navController: NavHostController) {
    when (bottomNav) {
        MainNav.BottomNav.Game -> GameCompose(modifier, navController = navController)
        MainNav.BottomNav.Mine -> MineCompose(modifier, navController = navController)
    }
}

@Composable
fun MainBottomBar(
    navs: List<MainNav.BottomNav>,
    currentNav: MainNav.BottomNav,
    modifier: Modifier = Modifier,
    onItemClick: (MainNav.BottomNav) -> Unit = {},
) {
    Surface(
        modifier = modifier.fillMaxSize(),
        shadowElevation = 8.dp
    ) {
        LazyRow(
            modifier = Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceAround
        ) {
            items(items = navs) { item ->
                val selectedA = item == currentNav
                Column(
                    verticalArrangement = Arrangement.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable { onItemClick(item) },
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Icon(
                        imageVector = item.icon,
                        contentDescription = null,
                        Modifier.size(24.dp),
                        tint = if (selectedA) MaterialTheme.colorScheme.primary else Color.Black
                    )
                    Text(
                        text = item.name,
                        fontSize = 14.sp,
                        modifier = Modifier.padding(horizontal = 16.dp),
                        color = if (selectedA) MaterialTheme.colorScheme.primary else Color.Black
                    )
                }
            }
        }
    }
}