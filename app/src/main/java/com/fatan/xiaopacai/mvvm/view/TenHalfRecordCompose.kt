package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.fatan.xiaopacai.mvvm.model.TenHalfMaxDbNum
import com.fatan.xiaopacai.mvvm.viewmodel.TenHalfRecordViewModel

/**
 * @author: WangYeTong
 * @date: 2024/1/3 11:00
 * @remake: 游戏十点半的历史记录
 */
@Composable
fun TenHalfRecordCompose(
    navHostController: NavHostController,
    tenHalfRecordViewModel: TenHalfRecordViewModel = viewModel()
) {
    val tenHalfEntityList by remember { tenHalfRecordViewModel.tenHalfEntityList }
    LaunchedEffect(key1 = Unit) {
        tenHalfRecordViewModel.getRecord()
    }

    Column {
        TitleBar(titleText = "历史记录最新${TenHalfMaxDbNum}条", backClick = {
            navHostController.navigateUp()
        })

        LazyColumn(contentPadding = PaddingValues(12.dp)) {
            itemsIndexed(tenHalfEntityList) { index, entity ->
                Column {
                    Box(modifier = Modifier.fillMaxWidth()) {
                        Text(text = "序号:${index + 1}", fontSize = 18.sp, color = Color.White)
                        Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.CenterEnd) {
                            Text(text = "${tenHalfRecordViewModel.getFormatDateString(entity.timeMill)}", fontSize = 18.sp, color = Color.White)
                        }
                    }
                    LazyRow(contentPadding = PaddingValues(12.dp)) {
                        itemsIndexed(entity.pukeList) { _, pukeItem ->
                            Image(
                                painter = painterResource(id = pukeItem.img),
                                contentDescription = null,
                                modifier = Modifier.width(120.dp)
                            )
                        }
                    }
                }
            }
        }
    }
}