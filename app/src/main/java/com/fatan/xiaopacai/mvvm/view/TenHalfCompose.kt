package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.fatan.xiaopacai.mvvm.model.GameList
import com.fatan.xiaopacai.mvvm.viewmodel.TenHalfViewModel

/**
 * @author: WangYeTong
 * @date: 2024/1/2 15:32
 * @remake: 游戏-10点半
 */
@Composable
fun TenHalfCompose(
    navHostController: NavHostController,
    tenHalfViewModel: TenHalfViewModel = viewModel()
) {
    val pukeDataList by remember { tenHalfViewModel.pukeDataList }
    val showDialog by remember { tenHalfViewModel.showDialog }
    val showDialogTip by remember { tenHalfViewModel.showDialogTip }
    if (showDialog) {
        AlertDialog(
            onDismissRequest = { tenHalfViewModel.setShowDialog(false) },
            text = {
                Text(text = showDialogTip)
            }, confirmButton = {
                Button(onClick = {
                    tenHalfViewModel.setShowDialog(false)
                }) {
                    Text(text = "OK")
                }
            })
    }
    Column {
        TitleBar(
            titleText = GameList.TenHalf.name,
            backClick = { navHostController.navigateUp() },
            rightText = "历史记录",
            rightClick = {
                navHostController.navigate(Nav.TenHalfRecord.route)
            }
        )
        LazyVerticalGrid(
            modifier = Modifier.weight(1f),
            columns = GridCells.Fixed(tenHalfViewModel.getGridCell()),
            contentPadding = PaddingValues(12.dp)
        ) {
            items(pukeDataList) { pukeItem ->
                Image(
                    painter = painterResource(id = pukeItem.img),
                    contentDescription = null
                )
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 50.dp)
        ) {
            Box(
                modifier = Modifier.weight(1f),
                contentAlignment = Alignment.Center
            ) {
                CircleButton(text = "抽") {
                    tenHalfViewModel.addRandomPuke()
                }
            }
            Box(
                modifier = Modifier.weight(1f),
                contentAlignment = Alignment.Center
            ) {
                CircleButton(text = "重新") {
                    tenHalfViewModel.reset()
                }
            }
        }
    }
}