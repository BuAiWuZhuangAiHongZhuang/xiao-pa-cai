package com.fatan.xiaopacai.mvvm.viewmodel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.fatan.xiaopacai.base.BaseViewModel
import com.fatan.xiaopacai.base.FORMAT_0
import com.fatan.xiaopacai.base.TAG
import com.fatan.xiaopacai.mvvm.model.RandomPuKeMaxDbNum
import com.fatan.xiaopacai.room.AppDataBase
import com.fatan.xiaopacai.room.randompuke.RandomPuKeEntity
import com.fatan.xiaopacai.util.JsonUtil
import java.text.SimpleDateFormat
import java.util.Locale

/**
 * @author: WangYeTong
 * @date: 2023/12/28 18:22
 * @remake:
 */
class RandomPuKeRecordViewModel : BaseViewModel() {
    private val _allRecordData = mutableStateOf<List<RandomPuKeEntity>>(listOf())
    val allRecordData: MutableState<List<RandomPuKeEntity>>
        get() = _allRecordData

    private val _dataFormat by lazy { SimpleDateFormat(FORMAT_0, Locale.getDefault()) }

    /**
     * 全部历史记录
     */
    fun getAllRecord() = serverAwait {
        val tempList = AppDataBase.database.randomPuKeDao().getAll().reversed()
        if (tempList.size > RandomPuKeMaxDbNum) {
            _allRecordData.value = tempList.subList(0, RandomPuKeMaxDbNum)
            //多出来的给删掉
            val externalList = tempList.subList(RandomPuKeMaxDbNum, tempList.size)
            externalList.forEach { item ->
                Log.i(TAG, "随机扑克删除数据:${JsonUtil.toJson(item)}")
                AppDataBase.database.randomPuKeDao().delete(item)
            }

        } else {
            _allRecordData.value = tempList
        }
        Log.i(TAG, "获取随机扑克全部记录:${JsonUtil.toJson(_allRecordData.value)}")
    }

    fun getFormatDateString(mill: Long): String? = _dataFormat.format(mill)

}