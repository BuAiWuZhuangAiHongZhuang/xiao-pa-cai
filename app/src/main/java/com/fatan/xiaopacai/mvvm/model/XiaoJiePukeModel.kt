package com.fatan.xiaopacai.mvvm.model

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue

/**
 * @author: WangYeTong
 * @date: 2024/1/8 18:07
 * @remake:
 */
class Player(
    val name: String = "",
    _pukeList: List<PuKe> = listOf()
) {
    var pukeList by mutableStateOf(_pukeList)
}

val nullPuke by lazy { PuKe(-1, "", 0, 0) }

/**
 * 小姐牌规则介绍里的Puke列表
 */
val pukeRulerList by lazy {
    listOf(
        allPuKe[1],
        allPuKe[5],
        allPuKe[9],
        allPuKe[13],
        allPuKe[17],
        allPuKe[21],
        allPuKe[25],
        allPuKe[29],
        allPuKe[33],
        allPuKe[37],
        allPuKe[41],
        allPuKe[45],
        allPuKe[49],
        allPuKe[52],
        allPuKe[53],
    )
}

/**
 * 获取以酒坑友的文字规则
 */
fun getRulerByPuke(puKe: PuKe): Pair<String, String> {
    return when (puKe.value) {
        1 -> Pair("点杀牌", "可指定任意一人喝酒")
        2 -> Pair("小姐牌", "大声说我是陪酒小妹，任何人喝酒必须陪一半，并且牵杯敬酒（大爷/大姐，您和好！），直到出现下一张2")
        3 -> Pair("爬天梯", "所有人一起石头剪刀布，赢者退出，输者继续，每出拳一次增加一杯，最后输的人全喝，人多可以半杯起加，三杯封顶")
        4 -> Pair("逛三园", "抽中者决定大概蔬菜、水果、动物，下一个人接着说，出错者罚酒")
        5 -> Pair("摄像机", "可保留此牌，任意时刻喊出“摄像机”，所有人一直重复前一秒的动作（或者不动），三秒内无人犯错则此卡无效")
        6 -> Pair("免死金牌", "可保留此牌，关键时刻用掉，抵挡一次喝酒")
        7 -> Pair(
            "逢7必过",
            "由抽排者开始，只能说10以内数字，敲击或者“过”则代表7，后面接着数数，遇带7和7的倍数不能说出，只能以敲击或者“过”代替。出错和跟错者都得受罚"
        )

        8 -> Pair("厕所牌", "本轮游戏开始后，只有拥有此牌者才能上厕所，也可赠送或出售此牌，比如帮我喝了这三杯酒")
        9 -> Pair("喝酒", "你瞅啥，自己喝")
        10 -> Pair("神经病", "大喊一声我是神经病，然后去找人说话吧，第一个搭理你的人喝酒")
        11 -> Pair("右边喝", "右边的人喝酒")
        12 -> Pair("左边喝", "左边的人喝酒")
        13 -> Pair("加酒牌", "第一个K喝一杯，然后决定下一个K喝多少杯")
        14 -> Pair("酒令卡", "此卡一出，所有人不能说“喝”、“酒”两个字，一个字罚一杯，谐音同样受罚。比如，我们都是90后。知道本轮游戏结束")
        15 -> Pair("国王牌", "在座的每个人都必须帮挡酒一杯")
        else -> Pair("未知", "未知呀")
    }
}