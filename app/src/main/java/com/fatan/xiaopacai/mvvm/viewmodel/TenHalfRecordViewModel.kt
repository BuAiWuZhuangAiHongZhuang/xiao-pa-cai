package com.fatan.xiaopacai.mvvm.viewmodel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.fatan.xiaopacai.base.BaseViewModel
import com.fatan.xiaopacai.base.FORMAT_0
import com.fatan.xiaopacai.base.TAG
import com.fatan.xiaopacai.mvvm.model.TenHalfMaxDbNum
import com.fatan.xiaopacai.room.AppDataBase
import com.fatan.xiaopacai.room.tenhalf.TenHalfEntity
import com.fatan.xiaopacai.util.JsonUtil
import java.text.SimpleDateFormat
import java.util.Locale

/**
 * @author: WangYeTong
 * @date: 2024/1/3 10:49
 * @remake:
 */
class TenHalfRecordViewModel : BaseViewModel() {
    private val _tenHalfEntityList = mutableStateOf<List<TenHalfEntity>>(listOf())
    val tenHalfEntityList: MutableState<List<TenHalfEntity>>
        get() = _tenHalfEntityList

    private val _dataFormat by lazy { SimpleDateFormat(FORMAT_0, Locale.getDefault()) }

    fun getRecord() = serverAwait {
        val tempList = AppDataBase.database.tenHalfDao().getAll().reversed()
        Log.i(TAG, "getRecord: tempList:${JsonUtil.toJson(tempList)}")
        _tenHalfEntityList.value = if (tempList.size > TenHalfMaxDbNum) tempList.subList(0, TenHalfMaxDbNum) else tempList
    }

    fun getFormatDateString(mill: Long): String? = _dataFormat.format(mill)

}