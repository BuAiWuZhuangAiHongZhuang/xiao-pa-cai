package com.fatan.xiaopacai.mvvm.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import com.fatan.xiaopacai.base.BaseViewModel
import com.fatan.xiaopacai.mvvm.model.gameList

/**
 * @author: WangYeTong
 * @date: 2023/12/26 17:10
 * @remake:
 */
class GameViewModel : BaseViewModel() {
    private val _colorList = listOf(Color.Red, Color.Yellow, Color.Blue, Color.DarkGray, Color.Gray, Color.LightGray, Color.Green, Color.Cyan, Color.Magenta)

    private val _randomColorList = mutableStateOf<List<Color>>(mutableListOf())
     val randomColorList: MutableState<List<Color>>
        get() = _randomColorList

    fun getRandomColorList() {
        _randomColorList.value = mutableListOf<Color>().apply {
            gameList.forEach { _ -> add(_colorList.random()) }
        }
    }
}