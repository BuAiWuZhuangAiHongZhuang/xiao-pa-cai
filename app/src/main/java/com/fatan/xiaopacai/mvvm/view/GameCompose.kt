package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fatan.xiaopacai.mvvm.model.GameList
import com.fatan.xiaopacai.mvvm.model.gameList
import com.fatan.xiaopacai.mvvm.viewmodel.GameViewModel
import com.fatan.xiaopacai.util.ToastUtil

/**
 * @author: WangYeTong
 * @date: 2023/12/26 17:23
 * @remake:
 */
@Composable
fun GameCompose(
    modifier: Modifier = Modifier,
    gameViewModel: GameViewModel = viewModel(),
    navController: NavHostController = rememberNavController()
) {
    val randomColorList by remember { gameViewModel.randomColorList }
    LaunchedEffect(key1 = Unit) {
        gameViewModel.getRandomColorList()
    }
    Column(modifier = modifier) {
        //标题栏
        TitleBar(isBack = false, titleText = "游戏")
        //游戏列表
        LazyColumn(contentPadding = PaddingValues(horizontal = 20.dp, vertical = 8.dp)) {
            itemsIndexed(gameList) { index, item ->
                GameCard(
                    color = if (randomColorList.size <= index) Color.Red else randomColorList[index],
                    item = item,
                    navController = navController
                )
            }
        }
    }
}

@Composable
fun GameCard(
    color: Color,
    item: GameList,
    navController: NavHostController
) {
    Card(
        modifier = Modifier
            .padding(vertical = 8.dp)
            .height(120.dp),
        colors = CardDefaults.cardColors(color),
        shape = RoundedCornerShape(6.dp)
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .clickable {
                    when (item.id) {
                        GameList.GameRandomShaiZi.id -> navController.navigate(Nav.RandomShaiZi.route)
                        GameList.GameRandomPuke.id -> navController.navigate(Nav.RandomPuKe.route)
                        GameList.GameMeiNvChanShen.id -> navController.navigate(Nav.MeiNvChanShen.route)
                        GameList.TenHalf.id -> navController.navigate(Nav.TenHalf.route)
                        GameList.GameXiaoJiePuke.id -> navController.navigate(Nav.XiaoJiePuke.route)
                        GameList.GameCaiQuan.id -> navController.navigate(Nav.CaiQuan.route)
                        GameList.GameWynmy.id -> navController.navigate(Nav.Wynmy.route)
                        GameList.GameQiBaJiu.id -> navController.navigate(Nav.QiBaJiu.route)
                        else -> ToastUtil.show("暂未实现")
                    }
                },
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = item.name,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White,
            )
        }
    }
}