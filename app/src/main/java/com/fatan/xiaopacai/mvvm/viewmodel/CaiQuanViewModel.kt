package com.fatan.xiaopacai.mvvm.viewmodel

import androidx.compose.runtime.MutableIntState
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import com.fatan.xiaopacai.base.BaseViewModel
import com.fatan.xiaopacai.mvvm.model.CaiQuanType
import com.fatan.xiaopacai.mvvm.model.caiQuanTypeList

/**
 * @author: WangYeTong
 * @date: 2024/1/18 14:26
 * @remake:
 */
class CaiQuanViewModel : BaseViewModel() {
    private val _type = mutableStateOf(caiQuanTypeList.random())
    val type: MutableState<CaiQuanType>
        get() = _type

    private val _randomNum = mutableIntStateOf(0)
    val randomNum: MutableIntState
        get() = _randomNum

    fun randomType() {
        val randomType = caiQuanTypeList.random()
        _type.value = randomType
        _randomNum.intValue = _randomNum.intValue + 1
    }
}