package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.fatan.xiaopacai.mvvm.model.GameList
import com.fatan.xiaopacai.mvvm.viewmodel.CaiQuanViewModel

/**
 * @author: WangYeTong
 * @date: 2024/1/18 14:26
 * @remake: 剪刀石头布
 */
@Composable
fun CaiQuanCompose(
    navHostController: NavHostController,
    caiQuanViewModel: CaiQuanViewModel = viewModel()
) {
    val caiQuanType by remember { caiQuanViewModel.type }
    val randomNum by remember { caiQuanViewModel.randomNum }
    Column {
        TitleBar(titleText = GameList.GameCaiQuan.name, backClick = {
            navHostController.navigateUp()
        })

        Box(modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 20.dp), contentAlignment = Alignment.Center) {
            Text(text = "第${randomNum}次猜拳", color = Color.White, fontSize = 32.sp)
        }
        Column(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(painter = painterResource(id = caiQuanType.img), contentDescription = null, tint = Color.White)
            Text(text = caiQuanType.name, color = Color.White, fontSize = 32.sp)
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 50.dp),
            contentAlignment = Alignment.Center
        ) {
            CircleButton(
                text = "猜",
                onClick = {
                    caiQuanViewModel.randomType()
                })
        }
    }
}