package com.fatan.xiaopacai.mvvm.model

/**
 * @author: WangYeTong
 * @date: 2024/1/3 10:52
 * @remake:
 */
/**
 * 十点半数据库中最大数据数
 */
const val TenHalfMaxDbNum = 100