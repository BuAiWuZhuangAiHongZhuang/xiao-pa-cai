package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fatan.xiaopacai.R
import com.fatan.xiaopacai.mvvm.model.maxShaiZiNum
import com.fatan.xiaopacai.mvvm.model.minShaiZiNum
import com.fatan.xiaopacai.mvvm.viewmodel.RandomShaiZiViewModel
import com.fatan.xiaopacai.ui.theme.themeColor

/**
 * @author: WangYeTong
 * @date: 2023/12/26 18:08
 * @remake: 随机摇骰子
 */
@Composable
fun RandomShaiZiCompose(
    randomShaiZiViewModel: RandomShaiZiViewModel = viewModel(),
    navController: NavHostController = rememberNavController()
) {
    val shaiZiNum by remember { randomShaiZiViewModel.shaiZiNum }
    val randomDataList by remember { randomShaiZiViewModel.randomDataList }

    Column(modifier = Modifier.fillMaxSize()) {
        TitleBar(
            titleText = "随机摇骰子",
            backClick = { navController.navigateUp() },
            rightText = "历史记录",
            rightClick = {
                navController.navigate(Nav.RandomShaiZiRecord.route)
            }
        )
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(30.dp)
        ) {
            IconButton(
                onClick = { randomShaiZiViewModel.shaiZiMinus() },
                modifier = Modifier.size(100.dp),
                enabled = shaiZiNum > minShaiZiNum
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.round_indeterminate_check_box_24),
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier.fillMaxSize()
                )
            }

            Box(modifier = Modifier.weight(1f), contentAlignment = Alignment.Center) {
                Text(
                    text = "$shaiZiNum",
                    fontSize = 60.sp,
                    color = Color.White,
                    fontWeight = FontWeight.Bold
                )
            }
            IconButton(
                onClick = { randomShaiZiViewModel.shaiZiAdd() },
                modifier = Modifier.size(100.dp),
                enabled = shaiZiNum < maxShaiZiNum
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
        //骰盅
        Box(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
        ) {
            //底盘和骰子
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.BottomCenter
            ) {
                ShaiZiColumn(modifier = Modifier.padding(30.dp), randomDataList = randomDataList)
            }
        }

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 50.dp),
            contentAlignment = Alignment.Center
        ) {
            CircleButton(text = "摇"){
                randomShaiZiViewModel.getRandomDataList()
                randomShaiZiViewModel.addRecord(randomDataList)
            }
        }
    }
}

@Composable
@Preview
fun RandomShaiZiPreview() {
    Surface(modifier = Modifier.fillMaxSize()) {
        RandomShaiZiCompose()
    }
}