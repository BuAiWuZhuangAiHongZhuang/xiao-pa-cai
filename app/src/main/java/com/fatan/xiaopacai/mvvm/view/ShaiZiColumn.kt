package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.fatan.xiaopacai.mvvm.model.getImgByShaiZiNum

/**
 * @author: WangYeTong
 * @date: 2023/12/27 17:16
 * @remake: 筛子列表控件
 */
@Composable
fun ShaiZiColumn(modifier: Modifier = Modifier, randomDataList: List<Int>) {
    LazyRow(
        contentPadding = PaddingValues(16.dp),
        horizontalArrangement = Arrangement.Center,
        modifier = modifier.fillMaxWidth()
    ) {
        items(randomDataList) {
            Image(
                painter = painterResource(id = getImgByShaiZiNum(it)),
                contentDescription = null,
                modifier = Modifier.padding(horizontal = 5.dp)
            )
        }
    }
}