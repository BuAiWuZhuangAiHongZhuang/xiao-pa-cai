package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.fatan.xiaopacai.ui.theme.themeColor

/**
 * @author: WangYeTong
 * @date: 2024/1/2 15:36
 * @remake:
 */
@Composable
fun TitleBar(
    modifier: Modifier = Modifier,
    isBack: Boolean = true,
    backClick: () -> Unit = {},
    titleText: String,
    rightText: String? = null,
    rightClick: () -> Unit = {},
) {
    Box(
        modifier = modifier
            .background(themeColor)
            .statusBarsPadding()
            .fillMaxWidth()
            .height(56.dp)
    ) {
        if (isBack) {
            IconButton(onClick = backClick, modifier = Modifier.size(56.dp)) {
                Icon(imageVector = Icons.Default.ArrowBack, contentDescription = null, tint = Color.White)
            }
        }
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(text = titleText, fontWeight = FontWeight.Bold, color = Color.White, fontSize = 18.sp)
        }

        if (!rightText.isNullOrEmpty()) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 12.dp),
                contentAlignment = Alignment.CenterEnd
            ) {
                Text(
                    text = rightText,
                    color = Color.White,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable(onClick = rightClick)
                )
            }
        }
    }
}