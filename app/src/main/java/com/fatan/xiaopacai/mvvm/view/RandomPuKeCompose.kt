package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fatan.xiaopacai.R
import com.fatan.xiaopacai.mvvm.model.maxPuKeNum
import com.fatan.xiaopacai.mvvm.model.minPuKeNum
import com.fatan.xiaopacai.mvvm.viewmodel.RandomPuKeViewModel
import com.fatan.xiaopacai.ui.theme.themeColor

/**
 * @author: WangYeTong
 * @date: 2023/12/28 16:46
 * @remake: 随机扑克
 */
@Composable
fun RandomPuKeCompose(
    randomPuKeViewModel: RandomPuKeViewModel = viewModel(),
    navController: NavHostController = rememberNavController()
) {
    val shaiZiNum by remember { randomPuKeViewModel.puKeNum }
    val randomPuKeDataList by remember { randomPuKeViewModel.puKeRandomDataList }

    Column(modifier = Modifier.fillMaxSize()) {
        //标题栏
        TitleBar(
            titleText = "随机扑克",
            backClick = { navController.navigateUp() },
            rightText = "历史记录"
        ) {
            navController.navigate(Nav.RandomPuKeRecord.route)
        }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(30.dp)
        ) {
            IconButton(
                onClick = { randomPuKeViewModel.puKeMinus() },
                modifier = Modifier.size(100.dp),
                enabled = shaiZiNum > minPuKeNum
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.round_indeterminate_check_box_24),
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier.fillMaxSize()
                )
            }

            Box(modifier = Modifier.weight(1f), contentAlignment = Alignment.Center) {
                Text(
                    text = "$shaiZiNum",
                    fontSize = 60.sp,
                    color = Color.White,
                    fontWeight = FontWeight.Bold
                )
            }
            IconButton(
                onClick = { randomPuKeViewModel.puKeAdd() },
                modifier = Modifier.size(100.dp),
                enabled = shaiZiNum < maxPuKeNum
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier.fillMaxSize()
                )
            }
        }

        Box(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            LazyHorizontalGrid(
                rows = GridCells.Fixed(randomPuKeViewModel.getGridCount()),
                horizontalArrangement = Arrangement.Center,
                modifier = Modifier.wrapContentWidth(),
                contentPadding = PaddingValues(12.dp)
            ) {
                itemsIndexed(randomPuKeDataList) { index, item ->
                    Image(
                        painter = painterResource(id = item.img),
                        contentDescription = null,
                        Modifier.size(150.dp)
                    )
                }
            }

        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 50.dp),
            contentAlignment = Alignment.Center
        ) {
            CircleButton(text = "摇") {
                randomPuKeViewModel.getRandomDataList()
                randomPuKeViewModel.addRecord(randomPuKeDataList)
            }
        }
    }
}

@Composable
@Preview
fun RandomPuKePreview() {
    Surface(modifier = Modifier.fillMaxSize()) {
        RandomPuKeCompose()
    }
}