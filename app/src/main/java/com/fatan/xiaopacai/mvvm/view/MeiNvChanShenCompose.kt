package com.fatan.xiaopacai.mvvm.view

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fatan.xiaopacai.base.TAG
import com.fatan.xiaopacai.mvvm.viewmodel.MeiNvChanShenViewModel
import com.fatan.xiaopacai.ui.theme.themeColor
import com.fatan.xiaopacai.util.ToastUtil

/**
 * @author: WangYeTong
 * @date: 2023/12/29 14:36
 * @remake:
 */
@Composable
fun MeiNvChanShenCompose(
    navHostController: NavHostController,
    meiNvChanShenViewModel: MeiNvChanShenViewModel = viewModel()
) {
    val chanList1 by remember { meiNvChanShenViewModel.puKeList1 }
    val chanList2 by remember { meiNvChanShenViewModel.puKeList2 }
    val chanList3 by remember { meiNvChanShenViewModel.puKeList3 }
    val showPukeFinishedDialog by remember { meiNvChanShenViewModel.showPukeFinishedDialog }
    if (showPukeFinishedDialog) {
        AlertDialog(
            text = {
                Text(text = "牌已发完，请点击下方缠按钮重新开始")
            },
            onDismissRequest = {
                meiNvChanShenViewModel.setShowPukeFinishedDialog(false)
            }, confirmButton = {
                Button(onClick = {
                }) {
                    meiNvChanShenViewModel.setShowPukeFinishedDialog(false)
                    Text(text = "OK")
                }
            })
    }
    Column {
        TitleBar(titleText = "美女缠身", backClick = { navHostController.navigateUp() })

        Box(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            Row(modifier = Modifier.padding(horizontal = 12.dp)) {
                Box(modifier = Modifier
                    .weight(1f)
                    .clickable {
                        meiNvChanShenViewModel.addPuKeList1()
                    }) {
                    for (index in chanList1.indices) {
                        val itemPuKe = chanList1[index]
                        Image(
                            painter = painterResource(id = itemPuKe.img),
                            contentDescription = null,
                            modifier = Modifier.padding(top = (60 * index).dp)
                        )
                    }
                }
                Box(modifier = Modifier
                    .weight(1f)
                    .clickable {
                        meiNvChanShenViewModel.addPuKeList2()
                    }) {
                    for (index in chanList2.indices) {
                        val itemPuKe = chanList2[index]
                        Image(
                            painter = painterResource(id = itemPuKe.img),
                            contentDescription = null,
                            modifier = Modifier.padding(top = (60 * index).dp)
                        )
                    }
                }
                Box(modifier = Modifier
                    .weight(1f)
                    .clickable {
                        meiNvChanShenViewModel.addPuKeList3()
                    }) {
                    for (index in chanList3.indices) {
                        val itemPuKe = chanList3[index]
                        Image(
                            painter = painterResource(id = itemPuKe.img),
                            contentDescription = null,
                            modifier = Modifier.padding(top = (60 * index).dp)
                        )
                    }
                }
            }
        }

        //缠
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 50.dp),
            contentAlignment = Alignment.Center
        ) {
            CircleButton(text = "缠") {
                meiNvChanShenViewModel.reset()
                meiNvChanShenViewModel.addPuKeList1()
                meiNvChanShenViewModel.addPuKeList2()
                meiNvChanShenViewModel.addPuKeList3()
            }
        }
    }
}

@Composable
@Preview
fun MeiNvChanShenPreview() {
    Surface(modifier = Modifier.fillMaxSize()) {
        MeiNvChanShenCompose(rememberNavController())
    }
}