package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.fatan.xiaopacai.mvvm.model.GameList
import com.fatan.xiaopacai.mvvm.model.qibaJiuGameRuler
import com.fatan.xiaopacai.mvvm.viewmodel.QiBaJiuViewModel

/**
 * @author: WangYeTong
 * @date: 2024/1/18 15:15
 * @remake: 七八九
 */
@Composable
fun QiBaJiuCompose(
    navHostController: NavHostController,
    qiBaJiuViewModel: QiBaJiuViewModel = viewModel()
) {
    Column {
        TitleBar(titleText = GameList.GameQiBaJiu.name, backClick = {
            navHostController.navigateUp()
        })
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(12.dp), contentAlignment = Alignment.Center
        ) {
            Text(text = qibaJiuGameRuler, color = Color.White, fontSize = 22.sp, textAlign = TextAlign.Center)
        }
    }
}