package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.fatan.xiaopacai.mvvm.model.pukeRulerList
import com.fatan.xiaopacai.mvvm.viewmodel.XiaoJiePukeRulerViewModel

/**
 * @author: WangYeTong
 * @date: 2024/1/9 16:08
 * @remake: 小姐牌-规则介绍
 */
@Composable
fun XiaoJiePukeRulerCompose(
    navHostController: NavHostController,
    xiaoJiePukeRulerViewModel: XiaoJiePukeRulerViewModel = viewModel()
) {
    Column {
        TitleBar(titleText = "小姐牌规则介绍", backClick = { navHostController.navigateUp() })
        LazyVerticalStaggeredGrid(columns = StaggeredGridCells.Fixed(2)) {
            items(pukeRulerList) {
                XiaoJiePuke(puKe = it)
            }
        }
    }
}