package com.fatan.xiaopacai.mvvm.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fatan.xiaopacai.mvvm.model.RandomShaiZiMaxDbNum
import com.fatan.xiaopacai.mvvm.viewmodel.RandomShaiZiRecordViewModel

/**
 * @author: WangYeTong
 * @date: 2023/12/27 17:05
 * @remake: 随机摇骰子记录
 */
@Composable
fun RandomShaiZiRecordCompose(
    navHostController: NavHostController,
    randomShaiZiRecordViewModel: RandomShaiZiRecordViewModel = viewModel()
) {
    val randomShaiZiDataList by remember { randomShaiZiRecordViewModel.randomShaiZiRecordDataList }
    LaunchedEffect(key1 = Unit) {
        randomShaiZiRecordViewModel.getAllRecordData()
    }
    Column(Modifier.fillMaxSize()) {
        TitleBar(
            titleText = "历史记录最新${RandomShaiZiMaxDbNum}条",
            backClick = { navHostController.navigateUp() }
        )
        LazyColumn(contentPadding = PaddingValues(15.dp)) {
            itemsIndexed(randomShaiZiDataList) { index, item ->
                Column(modifier = Modifier.fillMaxWidth()) {
                    Box(modifier = Modifier.padding(horizontal = 30.dp)) {
                        Text(text = "序号:${index + 1}", color = Color.White, fontSize = 18.sp)
                        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.CenterEnd) {
                            Text(
                                text = "${randomShaiZiRecordViewModel.getFormatDateString(item.timeMill)}",
                                color = Color.White,
                                fontSize = 18.sp,
                            )
                        }
                    }
                    ShaiZiColumn(
                        modifier = Modifier.padding(horizontal = 30.dp, vertical = 15.dp),
                        randomDataList = item.shaiZiList
                    )
                }
            }
        }
    }
}


@Composable
@Preview
fun RandomShaiZiRecordPreview() {
    Surface(modifier = Modifier.fillMaxSize()) {
        RandomShaiZiRecordCompose(rememberNavController())
    }
}