package com.fatan.xiaopacai.mvvm.model

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountCircle
import androidx.compose.material.icons.rounded.Home
import androidx.compose.material.icons.rounded.Person
import androidx.compose.material.icons.rounded.Settings
import androidx.compose.material.icons.rounded.ShoppingCart
import androidx.compose.ui.graphics.vector.ImageVector

/**
 * @author: WangYeTong
 * @date: 2023/12/20 10:53
 * @remake:
 */
object MainNav {
    sealed class BottomNav(val name: String, val icon: ImageVector) {
        object Game : BottomNav("游戏", Icons.Rounded.Home)
        object Mine : BottomNav("我的", Icons.Rounded.Person)
    }
}