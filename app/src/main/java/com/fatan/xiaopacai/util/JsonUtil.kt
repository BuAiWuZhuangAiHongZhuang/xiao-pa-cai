package com.fatan.xiaopacai.util

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonParser

/**
 * @author: WangYeTong
 * @date: 2023/12/22 17:48
 * @remake:
 */
object JsonUtil {
    val mGson by lazy { Gson() }

    /**
     * 转成list
     * 解决泛型问题
     * @param json
     * @param cls
     * @param <T>
     * @return
    </T> */
    fun <T> jsonToList(json: String?, cls: Class<T>?): List<T> {
        val list: MutableList<T> = ArrayList()
        val array: JsonArray = JsonParser().parse(json).asJsonArray
        for (elem in array) {
            list.add(mGson.fromJson(elem, cls))
        }
        return list
    }

    fun <T : Any> jsonToObj(json: String?, clazz: Class<T>): T {
        return mGson.fromJson(json, clazz)
    }

    fun toJson(obj: Any?): String? {
        return mGson.toJson(obj)
    }
}