package com.fatan.xiaopacai.util

import android.widget.Toast
import com.fatan.xiaopacai.base.BaseApplication

/**
 * @author: WangYeTong
 * @date: 2023/12/22 11:10
 * @remake:
 */
object ToastUtil {
    fun show(msg: Any?) {
        Toast.makeText(BaseApplication.INSTANCE, msg.toString(), Toast.LENGTH_SHORT).show()
    }
}